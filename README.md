# Configuration

Add access control in ``security.yaml``:

```
security:
    access_control:
        - { path: ^/kalitics/support/gitlab/hook, role: IS_AUTHENTICATED_ANONYMOUSLY }
```

Add routes control in ``config/routes/``:

```
_kalitics_support:
    resource: '@KaliticsSupportBundle/Resources/config/routes.xml'
    prefix: /kalitics/support/
```


Add twig alias in ``config/twig.yaml``:

```
twig:
    paths:
        '%kernel.project_dir%/vendor/kalitics/support-bundle/src/Resources/views': kaliticsSupport
```

The project must contain a twig function ``is_granted_in_context`` to grant access to the configuration and a constant corresponding at a super admin : ``App\\Enum\\Security\\RoleEnum::ROLE_SUPER_ADMIN``

```    
{% if is_granted_in_context(constant("App\\Enum\\Security\\RoleEnum::ROLE_SUPER_ADMIN")) %}
```

# Utilisation

Execute the command to synchronize all projects present in database table : 

``
php bin/console kalitics:support:synchronize
``

Configure via interface or in database (tables stars with ``support_``). 

To include the support function, render the action button in project view :

``
{{render(controller('kalitics_support.controller.submission_controller:displayAction', {'request' : app.request}))}}
``
