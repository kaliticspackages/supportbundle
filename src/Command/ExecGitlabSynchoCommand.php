<?php

namespace Kalitics\SupportBundle\Command;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Kalitics\SupportBundle\Entity\ProjectList;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class ExecGitlabSynchoCommand
 * @package Kalitics\SupportBundle\Command
 */
class ExecGitlabSynchoCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'kalitics:support:synchronize';

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * CreateUserCommand constructor.
     * @param $userSynchronisator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        HttpClientInterface $client
    ){
        parent::__construct();
        $this->manager = $entityManager;
        $this->client = $client;
    }

    protected function configure()
    {
        $this->setDescription('Call URL for all projects contained in database.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return bool|int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln('-- synchro Gitlab --');
        /** @var ProjectList $project */
        foreach($this->manager->getRepository(ProjectList::class)->findAll() as $project){
            $url = $project->getProjectUrl();

            $output->writeln($url);
            try {
                $response = $this->client->request('GET', $url);
            }catch (\Exception $exception){
                $output->writeln($exception->getMessage());
                continue;
            }
            $output->writeln($response->getContent());
        }

        $output->writeln('End');

        return true;
    }

}
