<?php

namespace Kalitics\SupportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Kalitics\SupportBundle\Repository\ProjectConfigurationRepository")
 * @ORM\Table(name="support_project_configuration")
*/
class ProjectConfiguration{

    /**
     * @var integer $id
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=250, nullable=false)
     */
    private $projectName;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $projectId;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $projectToken;

    /**
     * @var string
     * @ORM\Column(type="string", length=250, nullable=false)
     */
    private $senderEmail;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $copyEmails;

    /**
     * ProjectConfiguration constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function getProjectToken(): string
    {
        return $this->projectToken ?? '';
    }

    /**
     * @param string $projectToken
     */
    public function setProjectToken(string $projectToken): void
    {
        $this->projectToken = $projectToken;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getProjectId(): int
    {
        return $this->projectId;
    }

    /**
     * @param int $projectId
     */
    public function setProjectId(int $projectId): void
    {
        $this->projectId = $projectId;
    }

    /**
     * @return string
     */
    public function getProjectName(): string
    {
        return $this->projectName;
    }

    /**
     * @param string $projectName
     */
    public function setProjectName(string $projectName): void
    {
        $this->projectName = $projectName;
    }

    /**
     * @return string
     */
    public function getSenderEmail(): string
    {
        return $this->senderEmail;
    }

    /**
     * @param string $senderEmail
     */
    public function setSenderEmail(string $senderEmail): void
    {
        $this->senderEmail = $senderEmail;
    }

    /**
     * @return string
     */
    public function getCopyEmails(): ?string
    {
        return $this->copyEmails;
    }

    /**
     * @param string $copyEmails
     */
    public function setCopyEmails(string $copyEmails): void
    {
        $this->copyEmails = $copyEmails;
    }
}
