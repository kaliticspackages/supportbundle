<?php

namespace Kalitics\SupportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="support_project_configuration_list_urls")
*/
class ProjectList{

    /**
     * @var integer $id
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=250, nullable=false)
     */
    private $projectUrl;

    /**
     * ProjectConfiguration constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function getProjectUrl(): string
    {
        return $this->projectUrl;
    }

    /**
     * @param string $projectUrl
     */
    public function setProjectUrl(string $projectUrl): void
    {
        $this->projectUrl = $projectUrl;
    }
}
