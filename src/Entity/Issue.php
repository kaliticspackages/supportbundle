<?php

namespace Kalitics\SupportBundle\Entity;

use App\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gitlab\Api\Issues;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Kalitics\SupportBundle\Repository\IssueRepository")
 * @ORM\Table(name="support_issues")
 */
class Issue
{

    const TYPE_NEW          = 'new';
    const TYPE_SUPPORT      = 'support';
    const TYPE_BUG          = 'bug';

    const LABEL_TO_CLARIFY          = 'wf::A clarifier';
    const WF_A_ARBITRER             = 'wf::16-à arbitrer';
    const WF_WAITING_DEV_ESTIMATE   = 'wf::17-à chiffrer DEV';
    const WF_WAITING_APPROVAL       = 'wf::18-à valider client';
    const WF_BACKLOG                = 'wf::20-backlog';

    /**
     * @var integer $id
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=250, nullable=false)
     */
    private $subject;

    /**
     * @var string
     * @ORM\Column(type="string", length=4000, nullable=true)
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(type="string", length=4000, nullable=true)
     */
    private $link;

    /**
     * @var string
     * @ORM\Column(type="string", length=250, nullable=false)
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(type="string", length=250)
     */
    private $status;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isUrgent;

    /**
     * @var
     * @ORM\Column(type="json", name="labels", nullable=true)
     */
    private $labels;

    /**
     * @var integer $gitlabId
     * @ORM\Column(type="integer", nullable=true)
     */
    private $gitlabId;

    /**
     * @var integer $iid
     * @ORM\Column(type="integer", nullable=true)
     */
    private $iid;

    /**
     * @var string $gitlabUrl
     * @ORM\Column(type="string", nullable=true)
     */
    private $gitlabUrl;

    /**
     * @var string $gitlabImageUrl
     * @ORM\Column(type="string", nullable=true)
     */
    private $gitlabImageUrl;

    /**
     * @var string $gitlabImageMarkdown
     * @ORM\Column(type="string", nullable=true)
     */
    private $gitlabImageMarkdown;

    /**
     * @var string $gitlabImageMarkdown
     * @ORM\Column(type="string", nullable=true)
     */
    private $gitlabHumanTimeEstimate;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $informationsPosted;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    private $validatedBy;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $validatedAt;

    /**
     * Issue constructor.
     * @param User $createdBy
     * @throws \Exception
     */
    public function __construct(?User $createdBy = null)
    {

        $this->createdBy = $createdBy;

        $this->setCreatedAt(new \DateTime("now"));
        $this->setUpdatedAt(new \DateTime("now"));
        $this->setStatus(Issues::STATE_OPENED);

        $this->setSubject("");
        $this->setDescription("");
        $this->setType("");
        $this->setLink("");
        $this->setIsUrgent(false);
        $this->setInformationsPosted(false);
        $this->setIid(0);

        //Par défaut un tiquet n'est jamais validé
        $this->setValidatedAt(null);
        $this->setValidatedBy(null);
        $this->setGitlabHumanTimeEstimate(null);
    }

    /**
     * @return string
     */
    public function getGitlabHumanTimeEstimate(): string
    {
        return $this->gitlabHumanTimeEstimate ?? "Pas d'estimation";
    }

    /**
     * @param string $gitlabHumanTimeEstimate
     */
    public function setGitlabHumanTimeEstimate($gitlabHumanTimeEstimate): void
    {
        $this->gitlabHumanTimeEstimate = $gitlabHumanTimeEstimate;
    }

    /**
     * @param $label
     * @return $this
     */
    public function addLabel($label){

        $issueLabels = $this->getLabels();

        if(!in_array($label, $issueLabels)){
            $issueLabels[] = $label;
            $this->setLabels($issueLabels);
        }

        return $this;

    }

    /**
     * @param $label
     * @return $this
     */
    public function removeLabel($label){
        $issueLabels = $this->getLabels();

        if(in_array($label, $issueLabels)){
            $labelKey = array_search($label, $issueLabels);
            unset($issueLabels[$labelKey]);
            $this->setLabels($issueLabels);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValidatedBy()
    {
        return $this->validatedBy;
    }

    /**
     * @param mixed $validatedBy
     */
    public function setValidatedBy($validatedBy): void
    {
        $this->validatedBy = $validatedBy;
    }

    /**
     * @return mixed
     */
    public function getValidatedAt()
    {
        return $this->validatedAt;
    }

    /**
     * @param mixed $validatedAt
     */
    public function setValidatedAt($validatedAt): void
    {
        $this->validatedAt = $validatedAt;
    }

    /**
     * @return string
     */
    public function getGitlabImageMarkdown(): string
    {
        return (string)$this->gitlabImageMarkdown;
    }

    /**
     * @param string $gitlabImageMarkdown
     */
    public function setGitlabImageMarkdown(string $gitlabImageMarkdown): void
    {
        $this->gitlabImageMarkdown = $gitlabImageMarkdown;
    }

    /**
     * @return string|null
     */
    public function getGitlabImageUrl(): ?string
    {
        return $this->gitlabImageUrl;
    }

    /**
     * @param string $gitlabImageUrl
     */
    public function setGitlabImageUrl(?string $gitlabImageUrl): void
    {
        $this->gitlabImageUrl = $gitlabImageUrl;
    }

    /**
     * @return bool
     */
    public function isInformationsPosted(): bool
    {
        return $this->informationsPosted;
    }

    /**
     * @param bool $informationsPosted
     */
    public function setInformationsPosted(bool $informationsPosted): void
    {
        $this->informationsPosted = $informationsPosted;
    }

    /**
     * @return string
     */
    public function getGitlabUrl(): string
    {
        return $this->gitlabUrl;
    }

    /**
     * @param string $gitlabUrl
     */
    public function setGitlabUrl(string $gitlabUrl): void
    {
        $this->gitlabUrl = $gitlabUrl;
    }

    /**
     * @return int
     */
    public function getIid(): int
    {
        return $this->iid;
    }

    /**
     * @param int $iid
     */
    public function setIid(int $iid): void
    {
        $this->iid = $iid;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getGitlabId(): int
    {
        return $this->gitlabId;
    }

    /**
     * @param int $gitlabId
     */
    public function setGitlabId(int $gitlabId): void
    {
        $this->gitlabId = $gitlabId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isUrgent(): bool
    {
        return $this->isUrgent;
    }

    /**
     * @param bool $isUrgent
     */
    public function setIsUrgent(bool $isUrgent): void
    {
        $this->isUrgent = $isUrgent;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink($link): void
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @param $status
     */
    public function setStatus($status)
    {
        if (!in_array($status, array(Issues::STATE_OPENED, Issues::STATE_CLOSED))) {
            throw new \InvalidArgumentException("Invalid status");
        }
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return '#'.$this->getIid().' '.$this->getSubject();
    }

    /**
     * @return bool|null
     */
    public function getIsUrgent(): ?bool
    {
        return $this->isUrgent;
    }

    /**
     * @return bool|null
     */
    public function getInformationsPosted(): ?bool
    {
        return $this->informationsPosted;
    }

    /**
     * @return array|null
     */
    public function getLabels(): ?array
    {
        return $this->labels;
    }

    /**
     * @param array|null $labels
     * @return $this
     */
    public function setLabels(?array $labels): self
    {
        $this->labels = $labels;
        return $this;
    }

    /**
     * @param User|null $createdBy
     * @return $this
     */
    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;
        return $this;
    }

}
