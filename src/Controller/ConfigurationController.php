<?php

namespace Kalitics\SupportBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Kalitics\SupportBundle\Entity\Issue;
use Kalitics\SupportBundle\Entity\ProjectConfiguration;
use Kalitics\SupportBundle\Form\ProjectConfigurationType;
use Kalitics\SupportBundle\Form\SubmitIssueType;
use Kalitics\SupportBundle\Mailer\MailerServiceBuilder;
use Kalitics\SupportBundle\Service\GitLabComunicatorService;
use Kalitics\SupportBundle\Service\IssueManager;
use Kalitics\SupportBundle\Service\ProjectConfigurationManager;
use Kalitics\SupportBundle\Service\UserComunicatorService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\TwigBundle\DependencyInjection\Configuration;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ConfigurationController
 * @package Kalitics\SupportBundle\Controller
 */
class ConfigurationController extends Controller
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var IssueManager
     */
    private $issueManager;

    /**
     * @var ProjectConfigurationManager
     */
    private $projectConfigurationManager;

    /**
     * SubmissionController constructor.
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ProjectConfigurationManager $projectConfigurationManager,
        IssueManager $issueManager
    ){
        $this->manager = $entityManager;
        $this->projectConfigurationManager = $projectConfigurationManager;
        $this->issueManager = $issueManager;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function configure(Request $request){

        $projectConfiguration = $this->projectConfigurationManager->getProjectConfiguration();

        $form = $this->createForm(ProjectConfigurationType::class, $projectConfiguration, [
            'action' => $this->generateUrl('kalitics_support_configure', [])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $projectConfiguration = $form->getData();

            $this->manager->persist($projectConfiguration);
            $this->manager->flush();

            $request->getSession()->getFlashBag()->add('success', "Configuration mise à jours.");
            return $this->redirect($request->headers->get('referer'));
        }

        return $this->render('@KaliticsSupport/configuration/_configure.html.twig', [
            "form" => $form->createView()
        ]);
    }
}
