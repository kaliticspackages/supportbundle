<?php

namespace Kalitics\SupportBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Gitlab\Api\Issues;
use Kalitics\SupportBundle\Entity\Issue;
use Kalitics\SupportBundle\Form\SubmitIssueType;
use Kalitics\SupportBundle\Mailer\MailerServiceBuilder;
use Kalitics\SupportBundle\Service\GitLabComunicatorService;
use Kalitics\SupportBundle\Service\IssueManager;
use Kalitics\SupportBundle\Service\ProjectConfigurationManager;
use Kalitics\SupportBundle\Service\UserComunicatorService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\TwigBundle\DependencyInjection\Configuration;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Gitlab\Client as GitLabClient;
/**
 * Class SubmissionController
 * @package Kalitics\SupportBundle\Controller
 */
class SubmissionController extends Controller
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var IssueManager
     */
    private $issueManager;

    /**
     * @var ProjectConfigurationManager
     */
    private $projectConfigurationManager;

    /**
     * SubmissionController constructor.
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        IssueManager $issueManager,
        ProjectConfigurationManager $projectConfigurationManager
    ){
        $this->manager = $entityManager;
        $this->issueManager = $issueManager;
        $this->projectConfigurationManager = $projectConfigurationManager;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function displayAction()
    {
        return $this->render('@KaliticsSupport/_includes/_btn.html.twig', array());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction()
    {
        return $this->render('@KaliticsSupport/view.html.twig', array());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function modalAction()
    {
        return $this->render('@KaliticsSupport/_includes/_modal.html.twig', array());
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function list(Request $request){
        return $this->render('@KaliticsSupport/_includes/_list.html.twig', array(
            "issues" => $this->manager->getRepository(Issue::class)->findBy([], ['createdAt' => 'DESC'])
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function formAction(Request $request){

        $issue = new Issue($this->getUser());
        $form = $this->createForm(SubmitIssueType::class, $issue, [
            'action' => $this->generateUrl('kalitics_support_display_form', [])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($issue);
            $this->manager->flush();
            $this->issueManager->openIssue($issue, $form->get('file')->getData());
            $request->getSession()->getFlashBag()->add('success', "La demande à bien été soumise.");
            return $this->redirect($request->headers->get('referer'));
        }

        return $this->render('@KaliticsSupport/_includes/_form.html.twig', array(
            "form" => $form->createView(),
            'uri' => $request->getUri()
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editModalAction(Request $request, $id){

        /** @var Issue $issue */
        $issue = $this->manager->getRepository(Issue::class)->findOneBy(['gitlabId' => $id]);

        $form = $this->createForm(SubmitIssueType::class, $issue, [
            'action' => $this->generateUrl('kalitics_support_edit_issue', ['id' => $id])
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $issue = $form->getData();
            $this->manager->persist($issue);
            $this->manager->flush();

            $this->issueManager->updateIssue($issue);

            $request->getSession()->getFlashBag()->add('success', "La demande à bien été modifiée.");

            return $this->redirectToRoute('kalitics_support_view');
        }

        return $this->render('@KaliticsSupport/_includes/_edit_modal_content.html.twig', array(
            "form" => $form->createView(),
            "issue" => $issue
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function markClosedAction(Request $request, $id){
        /** @var Issue $issue */
        $issue = $this->manager->getRepository(Issue::class)->findOneBy(['gitlabId' => $id]);
        $this->issueManager->closeIssue($issue);
        $request->getSession()->getFlashBag()->add('success', "La demande à bien été cloturée.");
        return $this->redirectToRoute('kalitics_support_view');
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    private function generateForm(Request $request, Issue $issue){

        $form = $this->createForm(SubmitIssueType::class, $issue, [
            'action' => $this->generateUrl('kalitics_support_display_form', [])
        ]);

        return $form;
    }
}
