<?php

namespace Kalitics\SupportBundle\Controller;

use Doctrine\DBAL\Types\DateType;
use Doctrine\ORM\EntityManagerInterface;
use Gitlab\HttpClient\Builder;
use Http\Discovery\Exception\NotFoundException;
use Kalitics\SupportBundle\Entity\Issue;
use Kalitics\SupportBundle\Form\SubmitIssueType;
use Kalitics\SupportBundle\Mailer\MailerServiceBuilder;
use Kalitics\SupportBundle\Service\GitLabComunicatorService;
use Kalitics\SupportBundle\Service\IssueManager;
use Kalitics\SupportBundle\Service\UserComunicatorService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\TwigBundle\DependencyInjection\Configuration;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Gitlab\Client as GitLabClient;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class GitlabController
 * @package Kalitics\SupportBundle\Controller
 */
class GitlabController extends Controller
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var IssueManager
     */
    private $issueManager;

    /**
     * @var GitLabComunicatorService
     */
    private $gitLabComunicatorService;

    /**
     * SubmissionController constructor.
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        IssueManager $issueManager,
        GitLabComunicatorService $gitLabComunicatorService
    ){
        $this->manager = $entityManager;
        $this->issueManager = $issueManager;
        $this->gitLabComunicatorService = $gitLabComunicatorService;
    }


    /**
     * Url used for the cron to synchronize all issues
     *
     * @param Request $request
     * @return Response
     */
    public function gitLabHook(Request $request){
        $response = $this->issueManager->synchronizeIssues();
        return new Response($response['added'].' issues ajoutés, '.$response['updated'].' issues mises à jours, '.$response['removed'].' issues supprimées.', Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param $id
     * @return NotFoundException|\Symfony\Component\HttpFoundation\RedirectResponse|AccessDeniedException
     */
    public function validate(Request $request, $id){

        /** @var Issue $issue */
        $issue = $this->manager->getRepository(Issue::class)->find($id);
        if($issue === null){
            return new NotFoundException('Ticket introuvable');
        }

        if(!$this->getUser()->hasRole('support_validate')){
            return new AccessDeniedException("Vous n'etes pas authorisé à valider les tickets. Contactez un administrateur");
        }

        $this->issueManager->validateIssue($issue, $this->getUser());

        $this->addFlash('success', 'La demande à bien été validée et sera prise en charge dans les meilleurs délais');

        return $this->redirectToRoute('kalitics_support_view');
    }

    /**
     * @param Request $request
     * @param $id
     * @return NotFoundException|\Symfony\Component\HttpFoundation\RedirectResponse|AccessDeniedException
     */
    public function refuse(Request $request, $id){

        /** @var Issue $issue */
        $issue = $this->manager->getRepository(Issue::class)->find($id);
        if($issue === null){
            return new NotFoundException('Ticket introuvable');
        }

        if(!$this->getUser()->hasRole('support_validate')){
            return new AccessDeniedException("Vous n'etes pas authorisé à valider les tickets. Contactez un administrateur");
        }

        $now =  new \DateTime('now');
        $this->gitLabComunicatorService->postNote($issue, 'REFUSE par '.$this->getUser(). ' le '.$now->format('d/m/Y'));
        $this->issueManager->closeIssue($issue);

        $this->addFlash('success', 'La demande à correctement été clôturée');

        return $this->redirectToRoute('kalitics_support_view');
    }

}
