<?php

namespace Kalitics\SupportBundle\Form;

use Kalitics\SupportBundle\Entity\Issue;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThan;

class SubmitIssueType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('subject',TextType::class,[
                'label'         => 'Titre',
                'required'      => true
            ])
            ->add('type', ChoiceType::class, array(
                'label' => "Type de problème",
                'choices'  => array(
                    "Demande d'amélioration/modification"   => Issue::TYPE_NEW,
                    'Bug'                                   => Issue::TYPE_BUG,
                    'Autre demande'                         => Issue::TYPE_SUPPORT,
                ),
                'required'      => true,
            ))
            ->add('isUrgent', ChoiceType::class, array(
                'label' => "Bloquant",
                'choices'  => array(
                    'Non'      => false,
                    'Oui'      => true,
                )
            ))
            ->add('link', TextType::class, [
                'label'         => 'Page',
                'required'      => true,
                'attr' => ['readonly' => 'readonly'],
            ])
            ->add('description', TextareaType::class, [
                'label'         => 'Description',
                'attr'          => [
                    'rows' => '10'
                ],
                'required'      => true,
            ])
            ->add('file', FileType::class, [
                'label'         => 'Fichier',
                'mapped'        => false
            ])
        ;

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Issue::class,
            'allow_extra_fields' => true
        ]);
    }
}
