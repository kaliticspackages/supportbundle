<?php

namespace Kalitics\SupportBundle\Form;

use Kalitics\SupportBundle\Entity\Issue;
use Kalitics\SupportBundle\Entity\ProjectConfiguration;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThan;

class ProjectConfigurationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('projectName',TextType::class,[
                'label'         => 'Nom du projet',
                'required'      => true
            ])
            ->add('senderEmail',TextType::class,[
                'label'         => "Email d'envoi",
                'required'      => true
            ])
            ->add('copyEmails',TextType::class,[
                'label'         => 'Emails en cc',
                'required'      => true
            ])
            ->add('projectId',TextType::class,[
                'label'         => 'ID projet',
                'required'      => true
            ])
            ->add('projectToken',TextType::class,[
                'label'         => 'Token',
                'required'      => true
            ])
        ;

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProjectConfiguration::class,
            'allow_extra_fields' => true
        ]);
    }
}
