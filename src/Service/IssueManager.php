<?php

namespace Kalitics\SupportBundle\Service;

use App\Entity\User\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Gitlab\Api\Issues;
use Gitlab\Client as GitLabClient;
use Kalitics\SupportBundle\Entity\Issue;
use Kalitics\SupportBundle\Entity\ProjectConfiguration;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class IssueManager
 * @package Kalitics\SupportBundle\Service
 */
class IssueManager
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var UserComunicatorService
     */
    private $userComunicatorService;

    /**
     * @var GitLabComunicatorService
     */
    private $gitLabComunicatorService;

    /**
     * @var null
     */
    private $gitlabClient = null;

    /**
     * SubmissionController constructor.
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        GitLabComunicatorService $gitLabComunicatorService,
        UserComunicatorService $userComunicatorService
    ){
        $this->manager = $entityManager;
        $this->userComunicatorService = $userComunicatorService;
        $this->gitLabComunicatorService = $gitLabComunicatorService;
    }

    /**
     * @param Issue $issue
     * @return Issue
     */
    public function openIssue(Issue $issue, ?UploadedFile $file){

        $issue->setStatus(Issues::STATE_OPENED);
        $this->manager->persist($issue);
        $this->manager->flush();

        //Post du bug sur GitLab
        $this->gitLabComunicatorService->postIssueOnGitLab($issue, $file);

        //Envoi d'un email aux personnes configurés ainsi qu'a l'utilisateur
        $this->userComunicatorService->sendIssueCreatedEmail($issue, true);

        return $issue;
    }

    /**
     * @param Issue $issue
     * @return Issue
     */
    public function updateIssue(Issue $issue){

        $issue->removeLabel(Issue::LABEL_TO_CLARIFY);

        $this->manager->persist($issue);
        $this->manager->flush();

        //Post du bug sur GitLab
        return $this->gitLabComunicatorService->updateIssueOnGitLab($issue);
    }

    /**
     * @param Issue $issue
     * @return Issue
     */
    public function validateIssue(Issue $issue, User $validator){

        $now = new \DateTime('now');

        $issue->setValidatedBy($validator);
        $issue->setValidatedAt($now);

        $issue->removeLabel(Issue::WF_WAITING_APPROVAL);
        $issue->addLabel(Issue::WF_BACKLOG);

        $this->manager->persist($issue);
        $this->manager->flush();

        $this->gitLabComunicatorService->updateIssueOnGitLab($issue);
        $this->gitLabComunicatorService->postNote($issue, 'VALIDE par '.$validator. ' le '.$now->format('d/m/Y'));

        return $issue;
    }

    /**
     * @param Issue $issue
     * @return Issue
     */
    public function closeIssue(Issue $issue){

        $issue->setStatus(Issues::STATE_CLOSED);

        $issue->removeLabel(Issue::LABEL_TO_CLARIFY);
        $issue->removeLabel(Issue::WF_WAITING_APPROVAL);
        $issue->removeLabel(Issue::WF_A_ARBITRER);
        $issue->removeLabel(Issue::WF_WAITING_DEV_ESTIMATE);

        $this->manager->persist($issue);
        $this->manager->flush();

        $this->gitLabComunicatorService->closeGitLabIssue($issue);

        //Envoi d'un email aux personnes configurés ainsi qu'a l'utilisateur
        $this->userComunicatorService->sendIssueClosedEmail($issue, true);

        return $issue;
    }


    /**
     * @return int[]
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function synchronizeIssues(){

        $newIssuesCount = 0;
        $updatedIssuesCount = 0;
        $removedIssuesCount = 0;
        $now = new \DateTime();

        foreach($this->gitLabComunicatorService->getGitLabIssues() as $gitlablIssue){

            /** @var Issue $issue */
            $issue = $this->manager->getRepository(Issue::class)->findOneBy([
                'gitlabId' => $gitlablIssue['id']
            ]);

            if($issue === null){
                $issue = new Issue();
                $newIssuesCount++;
            }else {
                if($issue->getStatus() !== $gitlablIssue['state'] && $gitlablIssue['state'] === Issues::STATE_CLOSED && $issue->getCreatedBy() !== null){
                    $this->userComunicatorService->sendIssueClosedEmail($issue);
                    $removedIssuesCount++;
                }else{
                    $updatedIssuesCount++;
                }
            }

            //Cas d'une demande a clarifier par l'utilisateur a son origine
            $sendClarificationEmail = false;
            if(
                ($issue->getLabels() !== null && $gitlablIssue['labels'] !== null) &&
                !in_array(Issue::LABEL_TO_CLARIFY, $issue->getLabels()) &&
                in_array(Issue::LABEL_TO_CLARIFY, $gitlablIssue['labels'])
            ){
                $sendClarificationEmail = true;
            }

            //Cas d'une demande a faire valider par un responsable
            $sendApprovalRequiredEmail =  false;
            if(
                ($issue->getLabels() !== null && $gitlablIssue['labels'] !== null) &&
                !in_array(Issue::WF_WAITING_APPROVAL, $issue->getLabels()) &&
                in_array(Issue::WF_WAITING_APPROVAL, $gitlablIssue['labels']) &&
                !$sendClarificationEmail
            ){
                $sendApprovalRequiredEmail =  true;
            }

            $description = $gitlablIssue['description'];
            $description = str_replace('!'.$issue->getGitlabImageMarkdown(), '', $description);

            $issue->setSubject($gitlablIssue['title']);
            $issue->setIid($gitlablIssue['iid']);
            $issue->setGitlabUrl($gitlablIssue['web_url']);
            $issue->setGitlabId($gitlablIssue['id']);
            $issue->setDescription($description);
            $issue->setStatus($gitlablIssue['state']);
            $issue->setCreatedAt(new \DateTime($gitlablIssue['created_at']));
            $issue->setUpdatedAt($now);
            $issue->setLabels($gitlablIssue['labels']);
            $issue->setGitlabHumanTimeEstimate($gitlablIssue['time_stats']['human_time_estimate']);

            if(in_array(GitLabComunicatorService::GIT_LABEL_RISK_HIGH, $gitlablIssue['labels'])){
                $issue->setIsUrgent(true);
            }else{
                $issue->setIsUrgent(false);
            }

            if(in_array(GitLabComunicatorService::GIT_LABEL_TYPE_SUPPORT, $gitlablIssue['labels'])){
                $issue->setType(Issue::TYPE_SUPPORT);
            }elseif(in_array(GitLabComunicatorService::GIT_LABEL_TYPE_BUG, $gitlablIssue['labels'])){
                $issue->setType(Issue::TYPE_BUG);
            }elseif(in_array(GitLabComunicatorService::GIT_LABEL_TYPE_NEW, $gitlablIssue['labels'])){
                $issue->setType(Issue::TYPE_NEW);
            }

            if($sendClarificationEmail){
                $this->userComunicatorService->sendIssueToClarifyEmail($issue);
            }

            if($sendApprovalRequiredEmail){
                $this->userComunicatorService->sendIssueToValidateEmail($issue);
            }

            $this->manager->persist($issue);
        }
        $this->manager->flush();

        $now->modify('-1 second');
        foreach($this->manager->getRepository(Issue::class)->getUpdatedBefore($now) as $issue){
            $this->manager->remove($issue);
            $removedIssuesCount++;
        }
        $this->manager->flush();

        return [
            'added' => $newIssuesCount,
            'updated' => $updatedIssuesCount,
            'removed' => $removedIssuesCount,
        ];
    }


}
