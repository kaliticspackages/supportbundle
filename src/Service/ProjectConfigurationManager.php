<?php

namespace Kalitics\SupportBundle\Service;

use App\Entity\User\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Kalitics\SupportBundle\Entity\Issue;
use Kalitics\SupportBundle\Entity\ProjectConfiguration;

/**
 * Class ProjectConfigurationManager
 * @package Kalitics\SupportBundle\Service
 */
class ProjectConfigurationManager
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * SubmissionController constructor.
     */
    public function __construct(
        EntityManagerInterface $entityManager
    ){
        $this->manager = $entityManager;
    }
    
    /**
     * @return ProjectConfiguration|object|null
     */
    public function getProjectConfiguration(){

        $projectConfiguration = $this->manager->getRepository(ProjectConfiguration::class)->findOneBy([]);

        if($projectConfiguration === null){
            $projectConfiguration = new ProjectConfiguration();
            $this->manager->persist($projectConfiguration);
            $this->manager->flush();
        }

        return $projectConfiguration;
    }

}
