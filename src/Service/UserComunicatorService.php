<?php

namespace Kalitics\SupportBundle\Service;

use App\Entity\Security\Role;
use App\Entity\User\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Kalitics\SupportBundle\Entity\Issue;
use Kalitics\SupportBundle\Entity\ProjectConfiguration;

/**
 * Class UserComunicatorService
 * @package Kalitics\SupportBundle\Service
 */
class UserComunicatorService
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var ProjectConfiguration
     */
    private $projectConfigurationManager;

    /**
     * @var MailerService
     */
    private $mailer;

    /**
     * @var \Twig_Environment
     */
    private $templating;

    /**
     * @var GitLabComunicatorService
     */
    private $gitLabComunicatorService;

    /**
     * UserComunicatorService constructor.
     * @param EntityManagerInterface $entityManager
     * @param ProjectConfigurationManager $projectConfigurationManager
     * @param MailerService $mailer
     * @param \Twig_Environment $templating
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ProjectConfigurationManager $projectConfigurationManager,
        MailerService $mailer,
        \Twig_Environment $templating,
        GitLabComunicatorService $gitLabComunicatorService
    ){
        $this->manager = $entityManager;
        $this->projectConfiguration = $projectConfigurationManager->getProjectConfiguration();
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->gitLabComunicatorService = $gitLabComunicatorService;
    }

    /**
     * @param Issue $issue
     * @param bool $addCc
     * @return bool|void
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sendIssueCreatedEmail(Issue $issue, bool $addCc = true){

        if($issue->getCreatedBy() !== null && $issue->getCreatedBy()->getEmail() !== null){
            $this->mailer->sendEmail(
                'Ticket ouvert : '.$issue->getSubject(),
                [$issue->getCreatedBy()->getEmail()],
                $this->templating->render('@KaliticsSupport/mail/email_user_new_issue.html.twig', [
                    "title"             => "Nouvelle demande : ".$issue->getType(),
                    "issue"             => $issue,
                    "projectName"       => $this->projectConfiguration->getProjectName(),
                ]),
                $addCc
            );

            $this->gitLabComunicatorService->postNote($issue, 'Email envoyé à '.$issue->getCreatedBy()->getEmail().' : Ticket ouvert');
            return true;
        }

        return false;
    }

    /**
     * @param Issue $issue
     * @param bool $addCc
     * @return bool|void
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sendIssueUpdatedEmail(Issue $issue, bool $addCc = true){

        if($issue->getCreatedBy() !== null && $issue->getCreatedBy()->getEmail() !== null){

            $title = "Mise à jours de la demande";
            $title = $issue->getIid() == null ? $title : $title.' #'.$issue->getIid();

            $this->mailer->sendEmail(
                'Ticket mis à jours : '.$issue->getSubject(),
                [$issue->getCreatedBy()->getEmail()],
                $this->templating->render('@KaliticsSupport/mail/email_user_new_issue.html.twig', [
                    "title"             => $title,
                    "issue"             => $issue,
                    "projectName"       => $this->projectConfiguration->getProjectName(),
                ]),
                $addCc
            );

            $this->gitLabComunicatorService->postNote($issue, 'Email envoyé à '.$issue->getCreatedBy()->getEmail().' : Mise à jours de la demande');
            return true;
        }

        return false;
    }

    /**
     * @param Issue $issue
     * @param bool $addCc
     * @return bool|void
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sendIssueClosedEmail(Issue $issue, bool $addCc = true){

        if($issue->getCreatedBy() !== null && $issue->getCreatedBy()->getEmail() !== null){

            $title = "Votre demande à été cloturée";
            $title = $issue->getIid() == null ? $title : $title.' #'.$issue->getIid();

            $this->mailer->sendEmail(
                'Ticket cloturé : '.$issue->getSubject(),
                [$issue->getCreatedBy()->getEmail()],
                $this->templating->render('@KaliticsSupport/mail/email_user_issue_closed.html.twig', [
                    "title"             => $title,
                    "issue"             => $issue,
                    "projectName"       => $this->projectConfiguration->getProjectName(),
                ]),
                $addCc
            );

            $this->gitLabComunicatorService->postNote($issue, 'Email envoyé à '.$issue->getCreatedBy()->getEmail().' : Demande cloturée');
            return true;
        }

        return false;
    }

    /**
     * @param Issue $issue
     * @param bool $addCc
     * @return bool|void
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sendIssueToClarifyEmail(Issue $issue, bool $addCc = true){

        if($issue->getCreatedBy() !== null && $issue->getCreatedBy()->getEmail() !== null){

            $title = "Nous avons besoin de plus d'informations";
            $title = $issue->getIid() == null ? $title : $title.' #'.$issue->getIid();

            $this->mailer->sendEmail(
                "Demande d'information : ".$issue->getSubject(),
                [$issue->getCreatedBy()->getEmail()],
                $this->templating->render('@KaliticsSupport/mail/email_user_issue_to_clarify.html.twig', [
                    "title"             => $title,
                    "issue"             => $issue,
                    "projectName"       => $this->projectConfiguration->getProjectName(),
                ]),
                $addCc
            );

            $this->gitLabComunicatorService->postNote($issue, 'Email envoyé à '.$issue->getCreatedBy()->getEmail().' : Besoin de clarification');
            return true;
        }

        return false;
    }

    /**
     * @param Issue $issue
     * @return bool|void
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sendIssueToValidateEmail(Issue $issue){

        if($issue->getGitlabHumanTimeEstimate() === null){
            return false;
        }


        $title = "Demande en attente de validation";
        $title = $issue->getIid() == null ? $title : $title.' #'.$issue->getIid();

        $this->mailer->sendEmail(
            "Demande en attente de validation : ".$issue->getSubject(),
            $this->getValidatorsEmails(),
            $this->templating->render('@KaliticsSupport/mail/email_user_issue_to_validate.html.twig', [
                "title"             => $title,
                "issue"             => $issue,
                "projectName"       => $this->projectConfiguration->getProjectName(),
            ]),
            false
        );

        foreach ($this->getValidatorsEmails() as $v){
            $this->gitLabComunicatorService->postNote($issue, 'Email envoyé à '.$v.' : Demande en attente de validation');
        }

        return true;
    }

    /**
     * @return User[]
     */
    private function getValidatorsEmails(){
        $emails = [];
        foreach($this->manager->getRepository(User::class)->findByRole('support_validate') as $user){
            $emails[] = $user->getEmail();
        }
        return $emails;
    }

}
