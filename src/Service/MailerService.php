<?php

namespace Kalitics\SupportBundle\Service;

use App\Entity\User\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Kalitics\SupportBundle\Entity\Issue;
use Kalitics\SupportBundle\Entity\ProjectConfiguration;

/**
 * Class MailerService
 * @package Kalitics\SupportBundle\Service
 */
class MailerService
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var ProjectConfiguration
     */
    private $projectConfiguration;

    /**
     * @var \Swift_Mailer
     */
    private $swiftMailer;

    /**
     * MailerService constructor.
     * @param EntityManagerInterface $entityManager
     * @param ProjectConfigurationManager $projectConfigurationManager
     * @param \Swift_Mailer $swift_Mailer
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ProjectConfigurationManager $projectConfigurationManager,
        \Swift_Mailer $swift_Mailer
    ){
        $this->manager = $entityManager;
        $this->projectConfiguration = $projectConfigurationManager->getProjectConfiguration();
        $this->swiftMailer = $swift_Mailer;
    }

    /**
     * @param string $subject
     * @param array $to
     * @param string $body
     */
    public function sendEmail(string $subject, array $to, string $body, bool $addCc = false){

        $to = $addCc ? $this->addCopy($to) : $to;

        $swiftMessage = new \Swift_Message();
        $swiftMessage
            ->setSubject($subject)
            ->setFrom($this->projectConfiguration->getSenderEmail())
            ->setTo($to)
            ->setBody($body,'text/html');
        $this->swiftMailer->send($swiftMessage);
    }

    /**
     * @param array $to
     * @return array
     */
    private function addCopy(array $to){
        $copyEmailsString = $this->projectConfiguration->getCopyEmails();
        $copyEmails = explode(';', $copyEmailsString);
        $copyEmails = array_map('trim', $copyEmails);
        
        return array_merge(
            $to,
            $copyEmails
        );
    }
}
