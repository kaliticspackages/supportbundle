<?php

namespace Kalitics\SupportBundle\Service;

use App\Entity\User\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Gitlab\Api\Issues;
use Gitlab\Api\Projects;
use Gitlab\Client as GitLabClient;
use Kalitics\SupportBundle\Entity\Issue;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * https://docs.gitlab.com/ee/api/issues.html
 *
 * Class GitLabComunicatorService
 * @package Kalitics\SupportBundle\Service
 */
class GitLabComunicatorService
{

    const GIT_LABEL_FINAL_USER      = 'Visible client';
    const GIT_LABEL_TYPE_BUG        = 'type::Bug';
    const GIT_LABEL_TYPE_NEW        = 'type::New request';
    const GIT_LABEL_TYPE_SUPPORT    = 'type::Support';
    const GIT_LABEL_RISK_HIGH       = 'Risk - Medium';

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var ProjectConfiguration
     */
    private $projectConfiguration;

    /**
     * @var MailerService
     */
    private $mailer;

    /**
     * @var \Twig_Environment
     */
    private $templating;

    /**
     * @var GitLabClient
     */
    private $gitlabClient;

    /**
     * GitLabComunicatorService constructor.
     * @param EntityManagerInterface $entityManager
     * @param ProjectConfigurationManager $projectConfigurationManager
     * @param MailerService $mailer
     * @param \Twig_Environment $templating
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ProjectConfigurationManager $projectConfigurationManager,
        MailerService $mailer,
        \Twig_Environment $templating
    ){
        $this->manager = $entityManager;
        $this->projectConfiguration = $projectConfigurationManager->getProjectConfiguration();
        $this->mailer = $mailer;
        $this->templating = $templating;

        $client = new GitLabClient();
        $client->authenticate($this->projectConfiguration->getProjectToken(), GitLabClient::AUTH_HTTP_TOKEN);
        $this->gitlabClient = $client;
    }


    /**
     * @param Issue|null $issue
     * @return array
     */
    public function getGitlabIssues(?Issue $issue = null){
        $parameters = ['labels' => self::GIT_LABEL_FINAL_USER];
        $id = null !== $issue ? ['id' => $issue->getGitlabId()] : [];
        return $this->gitlabClient->projects()->issues($this->projectConfiguration->getProjectId(), array_merge($parameters, $id));
    }

    /**
     * @param Issue $issue
     * @return Issue
     */
    public function postIssueOnGitLab(Issue $issue, ?UploadedFile $file){

        $response = $this->gitlabClient->issues()->create($this->projectConfiguration->getProjectId(), $this->getGitLabParameters($issue));

        $issue->setGitlabId($response['id']);
        $issue->setIid($response['iid']);

        $this->manager->persist($issue);
        $this->manager->flush();

        if($file !== null){
            $this->postImage($issue, $file);
        }

        $this->postIssueInformations($issue);

        return $issue;
    }

    /**
     * @param Issue $issue
     * @param UploadedFile|null $file
     */
    public function postImage(Issue $issue, UploadedFile $file){
        $response = $this->gitlabClient->projects()->uploadFile($this->projectConfiguration->getProjectId(), $file->getRealPath());

        $issue->setGitlabImageUrl($response['full_path']);
        $issue->setGitlabImageMarkdown($response['markdown']);

        $this->manager->persist($issue);
        $this->manager->flush();

        $parameters = $this->getGitLabParameters($issue);
        $parameters['description'] = $parameters['description'].' !'.$response['markdown'];

        $this->gitlabClient->issues()->update($this->projectConfiguration->getProjectId(), $issue->getIid(), $parameters);
    }

    /**
     * @param Issue $issue
     */
    public function postIssueInformations(Issue $issue){

        //Poste note with basic informations
        $noteBody = 'URL: '.$issue->getLink();
        $mail = $issue->getCreatedBy() !== null && $issue->getCreatedBy()->getEmail() !== null ? $issue->getCreatedBy()->getEmail() : '?';
        $phone = $issue->getCreatedBy() !== null && $issue->getCreatedBy()->getPhone() !== null ? $issue->getCreatedBy()->getPhone() : '?';
        $user = ' Soumis par : '.$issue->getCreatedBy().' (email '.$mail.' / Téléphone '.$phone.')';
        $noteBody = $noteBody.$user;
        $this->postNote($issue, $noteBody);
    }

    /**
     * @param Issue $issue
     * @param $note
     * @return mixed
     */
    public function postNote(Issue $issue, $note){
        return $this->gitlabClient->issues()->addNote($this->projectConfiguration->getProjectId(), $issue->getIid(), $note);
    }

    /**
     * @param Issue $issue
     * @return mixed
     */
    public function updateIssueOnGitLab(Issue $issue){
        return $this->gitlabClient->issues()->update($this->projectConfiguration->getProjectId(), $issue->getIid(), $this->getGitLabParameters($issue));
    }

    /**
     * @param Issue $issue
     * @return mixed
     */
    public function closeGitLabIssue(Issue $issue){
        $parameters = $this->getGitLabParameters($issue);
        $parameters['state_event'] = 'close';
        return $this->gitlabClient->issues()->update($this->projectConfiguration->getProjectId(), $issue->getIid(), $parameters);
    }

    /**
     * @param Issue $issue
     * @return array
     */
    private function getGitLabParameters(Issue $issue){

        $issueDescription = $issue->getGitlabImageMarkdown() == null ? $issue->getDescription() : $issue->getDescription().' !'.$issue->getGitlabImageMarkdown();

        return [
            'title' => $issue->getSubject(),
            'description' => $issueDescription,
            'labels' => $this->addGitIssueLabels($issue)
        ];
    }

    /**
     * @param Issue $issue
     * @return string
     */
    public function addGitIssueLabels(Issue $issue){
        $labels = self::GIT_LABEL_FINAL_USER.',';

        if($issue->isUrgent()){
            $labels .= self::GIT_LABEL_RISK_HIGH.',';
        }

        if ($issue->getType() == Issue::TYPE_NEW){
            $labels .= self::GIT_LABEL_TYPE_NEW.',';
        }elseif($issue->getType() == Issue::TYPE_BUG){
            $labels .= self::GIT_LABEL_TYPE_BUG.',';
        }else{
            $labels .= self::GIT_LABEL_TYPE_SUPPORT.',';
        }

        //Ajout des autres labels
        if($issue->getLabels() !== null){
            foreach($issue->getLabels() as $l){
                if(substr_count($labels, $l) == 0){
                    $labels .= $l.',';
                }
            }
        }

        return $labels;
    }
}
