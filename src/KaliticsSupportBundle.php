<?php

namespace Kalitics\SupportBundle;

use Kalitics\SupportBundle\DependencyInjection\KaliticsSupportExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class KaliticsSupportBundle extends Bundle
{
    /**
     * Overridden to allow for the custom extension alias.
     */
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new KaliticsSupportExtension();
        }
        return $this->extension;
    }
}
